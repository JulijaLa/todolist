package io.codelex.toDoList;

public class ToDoList {

    public static ToDoList[] toDoList;
    private String task;
    private Integer taskDay;
    private Integer taskMonth;
    private Integer taskYear;
    private boolean MarkTaskDone;
    private boolean MarkTaskInProcess;
    private boolean MarkTaskHighPriority;

    public ToDoList(String task, Integer taskDay, Integer taskMonth, Integer taskYear, boolean markTaskDone,
                    boolean markTaskInProcess, boolean markTaskHighPriority) {
        this.task = task;
        this.taskDay = taskDay;
        this.taskMonth = taskMonth;
        this.taskYear = taskYear;
        MarkTaskDone = markTaskDone;
        MarkTaskInProcess = markTaskInProcess;
        MarkTaskHighPriority = markTaskHighPriority;
    }

    public static ToDoList[] getToDoList() {
        return toDoList;
    }

    public static void setToDoList(ToDoList[] toDoList) {
        ToDoList.toDoList = toDoList;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Integer getTaskDay() {
        return taskDay;
    }

    public void setTaskDay(Integer taskDay) {
        this.taskDay = taskDay;
    }

    public Integer getTaskMonth() {
        return taskMonth;
    }

    public void setTaskMonth(Integer taskMonth) {
        this.taskMonth = taskMonth;
    }

    public Integer getTaskYear() {
        return taskYear;
    }

    public void setTaskYear(Integer taskYear) {
        this.taskYear = taskYear;
    }

    public boolean isMarkTaskDone() {
        return MarkTaskDone;
    }

    public void setMarkTaskDone(boolean markTaskDone) {
        MarkTaskDone = markTaskDone;
    }

    public boolean isMarkTaskInProcess() {
        return MarkTaskInProcess;
    }

    public void setMarkTaskInProcess(boolean markTaskInProcess) {
        MarkTaskInProcess = markTaskInProcess;
    }

    public boolean isMarkTaskHighPriority() {
        return MarkTaskHighPriority;
    }

    public void setMarkTaskHighPriority(boolean markTaskHighPriority) {
        MarkTaskHighPriority = markTaskHighPriority;
    }

    @Override
    public String toString() {
        return "Task{" + "to do: " +
                task + " " +
                taskDay + "/" +
                taskMonth + "/" +
                taskYear +
                '}';
    }
}




