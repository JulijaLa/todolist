package io.codelex.toDoList;

import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

public class ToDoListMethods {

    Scanner scanner = new Scanner(System.in).useDelimiter("\\n");
    

    public void addNewTask() {

        System.out.println("Enter new task: ");
        String newTask = scanner.next();
        System.out.println("Enter task date: day ");
        Integer day = scanner.nextInt();
        System.out.println("Enter task date: month ");
        Integer month = scanner.nextInt();
        System.out.println("Enter task date: year ");
        Integer year = scanner.nextInt();

        ToDoList toDoList = new ToDoList(newTask, day, month, year, false,
                false, false);

        TaskList.taskList.add(toDoList);
    }

    public void deleteTask() {                                   //2 method

        System.out.println("Enter task ");
        String task = scanner.next();

        boolean taskIsInTaskList = false;
        for (ToDoList toDoList : TaskList.taskList) {
            if (task.equals(toDoList.getTask())) {
                TaskList.taskList.remove(toDoList);
                taskIsInTaskList = true;
                break;
            }
        }
        if (taskIsInTaskList == false) {
            System.out.println("Task is not found");
        }
    }

    public void markTaskHighPriority() {                              //3 method

        System.out.println("Enter task ");
        String task = scanner.next();

        boolean taskIsInTaskList = false;
        for (ToDoList toDoList : TaskList.taskList) {
            if (task.equals(toDoList.getTask())) {

                toDoList.setMarkTaskHighPriority(true);
                taskIsInTaskList = true;
                break;
            }
        }
        if (taskIsInTaskList == false) {
            System.out.println("Task is not found");
        }
    }

    public void markTaskDone() {                                        //4 method

        System.out.println("Enter task ");
        String task = scanner.next();

        boolean taskIsInTaskList = false;
        for (ToDoList toDoList : TaskList.taskList) {
            if (task.equals(toDoList.getTask())) {

                toDoList.setMarkTaskDone(true);
                taskIsInTaskList = true;
                break;
            }
        }
        if (taskIsInTaskList == false) {
            System.out.println("Task is not found");
        }
    }

    public void markTaskInProcess() {                                            //5 method

        System.out.println("Enter task ");
        String task = scanner.next();

        boolean taskIsInTaskList = false;
        for (ToDoList toDoList : TaskList.taskList) {
            if (task.equals(toDoList.getTask())) {

                toDoList.setMarkTaskInProcess(true);
                taskIsInTaskList = true;
                break;
            }
        }
        if (taskIsInTaskList == false) {
            System.out.println("Task is not found");
        }
    }

    public void showAllTasks() {                                              //6 method
        for (ToDoList toDoList : TaskList.taskList) {
            System.out.println(toDoList);
        }
    }

    public void showHighPriorityTasks() {                                       //7 method
        for (ToDoList toDoList : TaskList.taskList) {
            if (toDoList.isMarkTaskHighPriority()) {
                System.out.println(toDoList);
            }
        }
    }

    public void showTasksDone() {                                            //8 method
        for (ToDoList toDoList : TaskList.taskList) {
            if (toDoList.isMarkTaskDone()) {
                System.out.println(toDoList);
            }
        }
    }

    public void showTasksInProcess() {                                       //9 method
        for (ToDoList toDoList : TaskList.taskList) {
            if (toDoList.isMarkTaskInProcess()) {
                System.out.println(toDoList);
            }
        }
    }

    public void markAllTasksDone() {                                      //10 method
        for (ToDoList toDoList : TaskList.taskList) {
            toDoList.setMarkTaskDone(true);
        }
    }

    public void deleteAllTasks() {                                       //11 method
            TaskList.taskList.clear();

    }
}
