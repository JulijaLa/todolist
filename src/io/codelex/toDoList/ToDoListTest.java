package io.codelex.toDoList;

import java.util.Scanner;

public class ToDoListTest {

    public static void main(String[] args) {

        ToDoListMethods toDoListMethods = new ToDoListMethods();
        Scanner scanner = new Scanner(System.in);

        boolean menu = true;
        while (menu) {

            System.out.println("1 - Add new Task in Task List");
            System.out.println("2 - Delete Task from Task List");
            System.out.println("3 - Mark Task as of High Priority");
            System.out.println("4 - Mark Task as Done");
            System.out.println("5 - Mark Task as in Process");
            System.out.println("6 - Show all Tasks");
            System.out.println("7 - Show Tasks of High Priority");
            System.out.println("8 - Show Tasks Done");
            System.out.println("9 - Show Tasks in Process");
            System.out.println("10 - Mark all Tasks Done");
            System.out.println("11 - Delete all Tasks");
            System.out.println("12 - Exit");

            int menuChoice = scanner.nextInt();

            switch (menuChoice) {
                case 1:
                    toDoListMethods.addNewTask();
                    break;

                case 2:
                    toDoListMethods.deleteTask();
                    break;

                case 3:
                    toDoListMethods.markTaskHighPriority();
                    break;

                case 4:
                    toDoListMethods.markTaskDone();
                    break;

                case 5:
                    toDoListMethods.markTaskInProcess();
                    break;

                case 6:
                    toDoListMethods.showAllTasks();
                    break;

                case 7:
                    toDoListMethods.showHighPriorityTasks();
                    break;

                case 8:
                    toDoListMethods.showTasksDone();
                    break;

                case 9:
                    toDoListMethods.showTasksInProcess();
                    break;

                case 10:
                    toDoListMethods.markAllTasksDone();
                    break;

                case 11:
                    toDoListMethods.deleteAllTasks();
                    break;

                case 12:
                    menu = false;
                    break;

                default:
                    break;
            }

        }
    }
}

